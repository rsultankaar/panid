package com.panid.creational;

import static java.lang.String.format;
import static org.fest.assertions.api.Assertions.assertThat;

import com.panid.core.engine.CreationalPatternEngine;
import com.panid.core.engine.PatternEngine;
import com.panid.core.indexer.CreationalPatternIndexer;
import com.panid.core.scanner.PackageScannerImpl;
import com.panid.core.type.CreationalType;
import com.panid.pojos.prototype.PrototypeClonable;
import com.panid.pojos.prototype.PrototypeWithMethod;
import com.panid.pojos.prototype.PrototypeWithNoMethod;
import com.panid.pojos.prototype.PrototypeWithVoidMethod;
import java.util.NoSuchElementException;
import java.util.UUID;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class PrototypeTest {

  private static final String COM_PANID_POJOS = "com.panid.pojos.prototype";

  private static final String EXPECTED_EXCEPTION =
      "There was no method annotated "
          + "com.panid.creational.Prototype.PrototypeMethod.class "
          + "in %s.class. "
          + "Please annotate a non void method with @interface "
          + "com.panid.creational.Prototype$PrototypeMethod or implement clone() method.";
  @Rule public ExpectedException expectedException = ExpectedException.none();
  private PatternEngine patternsExecutor;

  @Before
  public void setUp() {
    var packageScanner = new PackageScannerImpl(COM_PANID_POJOS);
    var patternIndexer = new CreationalPatternIndexer(packageScanner);
    patternsExecutor = new CreationalPatternEngine(patternIndexer);
  }

  @Test
  public void must_return_different_object_with_same_param_via_clone_method() {
    var clonablePrototype = new PrototypeClonable(UUID.randomUUID());
    PrototypeClonable clonedPrototype = patternsExecutor.execute(clonablePrototype);

    assertThat(clonedPrototype).isNotEqualTo(clonablePrototype);
    assertThat(clonedPrototype.getUuid()).isEqualTo(clonablePrototype.getUuid());
  }

  @Test
  public void must_return_different_object_with_same_param_via_annotated_method() {
    PrototypeWithMethod clonablePrototype = new PrototypeWithMethod(UUID.randomUUID());
    PrototypeWithMethod clonedPrototype = patternsExecutor.execute(clonablePrototype);

    assertThat(clonedPrototype).isNotEqualTo(clonablePrototype);
    assertThat(clonedPrototype.getUuid()).isEqualTo(clonablePrototype.getUuid());
  }

  @Test
  public void must_throw_no_such_element_exception_due_to_no_annotated_method() {
    var prototypeNoMethod = new PrototypeWithNoMethod();

    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(
        format(EXPECTED_EXCEPTION, PrototypeWithNoMethod.class.getCanonicalName()));

    patternsExecutor.execute(prototypeNoMethod);
  }

  @Test
  public void must_throw_no_such_element_exception_due_to_void_annotated_method() {
    var prototypeNoMethod = new PrototypeWithVoidMethod();

    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(
        format(EXPECTED_EXCEPTION, PrototypeWithVoidMethod.class.getCanonicalName()));

    patternsExecutor.execute(prototypeNoMethod);
  }
}
