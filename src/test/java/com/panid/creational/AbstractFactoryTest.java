package com.panid.creational;

import static com.panid.core.executors.TargetPatternExecutor.ERROR_MESSAGE;
import static com.panid.core.executors.TargetPatternExecutor.WITH_TARGET;
import static com.panid.creational.Factory.FactoryMethod;
import static java.lang.String.format;
import static org.fest.assertions.api.Assertions.assertThat;

import com.panid.core.engine.CreationalPatternEngine;
import com.panid.core.engine.PatternEngine;
import com.panid.core.indexer.CreationalPatternIndexer;
import com.panid.core.scanner.PackageScannerImpl;
import com.panid.pojos.factory.StandardFactory;
import com.panid.pojos.factory.manufactuable.ProductA;
import com.panid.pojos.factory.manufactuable.ProductB;
import com.panid.pojos.factory.manufactuable.impl.ProductA1;
import com.panid.pojos.factory.manufactuable.impl.ProductA2;
import com.panid.pojos.factory.manufactuable.impl.ProductB1;
import com.panid.pojos.factory.manufactuable.impl.ProductB2;
import java.util.NoSuchElementException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class AbstractFactoryTest {

  private static final String COM_PANID_POJOS = "com.panid.pojos.factory";

  private PatternEngine patternEngine;

  @Rule public ExpectedException expectedException = ExpectedException.none();

  @Before
  public void setUp() {
    var packageScanner = new PackageScannerImpl(COM_PANID_POJOS);
    var patternIndexer = new CreationalPatternIndexer(packageScanner);
    patternEngine = new CreationalPatternEngine(patternIndexer);
  }

  @Test
  public void must_manufacture_product_A() {
    var factory = new StandardFactory();
    var productA1 = patternEngine.execute(factory, ProductA.class, true);
    var productA2 = patternEngine.execute(factory, ProductA.class, false);

    assertThat(productA1).isExactlyInstanceOf(ProductA1.class);
    assertThat(productA2).isExactlyInstanceOf(ProductA2.class);
  }

  @Test
  public void must_manufacture_product_B() {
    var factory = new StandardFactory();
    var productB1 = patternEngine.execute(factory, ProductB.class, true);
    var productB2 = patternEngine.execute(factory, ProductB.class, false);

    assertThat(productB1).isExactlyInstanceOf(ProductB1.class);
    assertThat(productB2).isExactlyInstanceOf(ProductB2.class);
  }

  @Test
  public void
      must_throw_NoSuchElementException_due_to_non_handled_class_in_factory_with_absent_method() {
    var factory = new StandardFactory();
    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(
        format(
            ERROR_MESSAGE + WITH_TARGET,
            FactoryMethod.class.getCanonicalName(),
            factory.getClass().getCanonicalName(),
            String.class.getCanonicalName()));

    patternEngine.execute(factory, String.class, true);
  }

  @Test
  public void
      must_throw_NoSuchElementException_due_to_non_handled_class_in_factory_but_method_is_present_and_annotated() {
    var factory = new StandardFactory();
    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(
        format(
            ERROR_MESSAGE + WITH_TARGET,
            FactoryMethod.class.getCanonicalName(),
            factory.getClass().getCanonicalName(),
            Double.class.getCanonicalName()));

    patternEngine.execute(factory, Double.class);
  }

  @Test
  public void
      must_throw_NoSuchElementException_due_to_handled_class_in_factory_with_method_present_but_not_annotated() {
    var factory = new StandardFactory();
    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(
        format(
            ERROR_MESSAGE + WITH_TARGET,
            FactoryMethod.class.getCanonicalName(),
            factory.getClass().getCanonicalName(),
            Integer.class.getCanonicalName()));

    patternEngine.execute(factory, Integer.class);
  }
}
