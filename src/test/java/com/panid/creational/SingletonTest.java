package com.panid.creational;

import static org.fest.assertions.api.Assertions.assertThat;

import com.panid.core.engine.CreationalPatternEngine;
import com.panid.core.engine.PatternEngine;
import com.panid.core.indexer.CreationalPatternIndexer;
import com.panid.core.scanner.PackageScannerImpl;
import com.panid.core.type.CreationalType;
import com.panid.pojos.singleton.BadSingleton;
import com.panid.pojos.singleton.SingletonType;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class SingletonTest {

  private static final String COM_PANID_POJOS = "com.panid.pojos.singleton";
  private static final String SINGLETON_EXCEPTION =
      "java.lang.NoSuchMethodException: "
          + "com.panid.pojos.singleton.BadSingleton.<init>(java.lang.String,java.lang.String)";

  @Rule public ExpectedException expectedException = ExpectedException.none();
  private PatternEngine patternsExecutor;

  @Before
  public void setUp() {
    var packageScanner = new PackageScannerImpl(COM_PANID_POJOS);
    var patternIndexer = new CreationalPatternIndexer(packageScanner);
    patternsExecutor = new CreationalPatternEngine(patternIndexer);
  }

  @Test
  public void must_create_singleton_when_called_twice_with_no_args() {
    var singleton1 = patternsExecutor.execute(SingletonType.class);
    var singleton2 = patternsExecutor.execute(SingletonType.class, "A useless argument");

    assertThat(singleton1).isEqualTo(singleton2);
  }

  @Test
  public void must_create_singleton_when_called_twice_with_args() {
    var singleton1 = patternsExecutor.execute(SingletonType.class, "With an argument");
    var singleton2 = patternsExecutor.execute(SingletonType.class);

    assertThat(singleton1).isEqualTo(singleton2);
  }

  @Test
  public void must_throw_RuntimeException_due_to_non_existing_constructor() {
    expectedException.expect(RuntimeException.class);
    expectedException.expectMessage(SINGLETON_EXCEPTION);

    patternsExecutor.execute(BadSingleton.class, "A String", "Another string.");
  }
}
