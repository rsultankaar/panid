package com.panid.creational;

import static java.lang.String.format;
import static org.fest.assertions.api.Assertions.assertThat;

import com.panid.core.engine.CreationalPatternEngine;
import com.panid.core.engine.PatternEngine;
import com.panid.core.indexer.CreationalPatternIndexer;
import com.panid.core.scanner.PackageScannerImpl;
import com.panid.creational.Builder.BuildMethod;
import com.panid.pojos.builder.GoodBuilder;
import com.panid.pojos.builder.NoMethodBuilder;
import com.panid.pojos.builder.VoidBuilder;
import java.util.NoSuchElementException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class BuilderTest {

  private static final String A_TEST_STRING = " a test string.";
  private static final String COM_PANID_POJOS = "com.panid.pojos.builder";

  private PatternEngine patternEngine;

  @Rule public ExpectedException expectedException = ExpectedException.none();

  @Before
  public void setUp() {
    var packageScanner = new PackageScannerImpl(COM_PANID_POJOS);
    var patternIndexer = new CreationalPatternIndexer(packageScanner);
    patternEngine = new CreationalPatternEngine(patternIndexer);
  }

  @Test
  public void must_build_object_without_args() {
    String builtString = patternEngine.execute(new GoodBuilder());
    assertThat(builtString).isEqualTo(GoodBuilder.HAS_BUILT_WITHOUT_STRING);
  }

  @Test
  public void must_build_object_with_null_args() {
    String builtString = patternEngine.execute(new GoodBuilder(), null);
    assertThat(builtString).isEqualTo(GoodBuilder.HAS_BUILT_WITHOUT_STRING);
  }

  @Test
  public void must_build_object_with_args() {
    String builtString = patternEngine.execute(new GoodBuilder(), A_TEST_STRING);
    assertThat(builtString).isEqualTo(GoodBuilder.HAS_BUILT_WITH_STRING + A_TEST_STRING);
  }

  @Test
  public void must_throw_IllegalArgumentException_due_too_many_args() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage("wrong number of arguments");

    patternEngine.execute(new GoodBuilder(), A_TEST_STRING, A_TEST_STRING);
  }

  @Test
  public void must_throw_IllegalArgumentException_due_to_wrong_argument_type() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage("argument type mismatch");

    patternEngine.execute(new GoodBuilder(), 50L);
  }

  @Test
  public void must_throw_IllegalArgumentException_due_void_return_type_method_in_builder() {
    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(
        format(
            "There was no method annotated %s.class in %s.class",
            BuildMethod.class.getCanonicalName(), VoidBuilder.class.getCanonicalName()));

    patternEngine.execute(new VoidBuilder());
  }

  @Test
  public void must_throw_NoSuchElementException_due_no_annotated_method_in_builder() {
    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(
        format(
            "There was no method annotated %s.class in %s.class",
            BuildMethod.class.getCanonicalName(), NoMethodBuilder.class.getCanonicalName()));

    patternEngine.execute(new NoMethodBuilder());
  }
}
