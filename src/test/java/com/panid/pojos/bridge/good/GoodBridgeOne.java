package com.panid.pojos.bridge.good;

import com.panid.structural.Bridge;
import com.panid.structural.Bridge.BridgeMethod;

@Bridge
public class GoodBridgeOne {

   public static final String DECORATION = "Decorate with GoodDecoratorOne.class. ";

   @BridgeMethod
   public void decorate(GoodBridgeTarget decoratee){
      decoratee.completeSentence(DECORATION);
   }

}
