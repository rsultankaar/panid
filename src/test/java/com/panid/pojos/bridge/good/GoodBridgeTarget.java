package com.panid.pojos.bridge.good;

import com.panid.structural.Bridge.BridgeTarget;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@BridgeTarget
public class GoodBridgeTarget {

   private final List<String> words;

   public GoodBridgeTarget() {
      this.words = new ArrayList<>();
   }

   public List<String> getWords() {
      return words;
   }

   public GoodBridgeTarget completeSentence(String sentence) {
      this.words.add(sentence);
      return this;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      GoodBridgeTarget decoratee = (GoodBridgeTarget) o;
      return Objects.equals(words, decoratee.words);
   }

   @Override
   public int hashCode() {
      return Objects.hash(words);
   }
}
