package com.panid.pojos.bridge.good;

import com.panid.structural.Bridge;
import com.panid.structural.Bridge.BridgeMethod;

@Bridge
public class GoodBridgeTwo {

   public static final String DECORATION = "Decoration with GoodDecoratorTwo.class.";
   private final String contextString;

   public GoodBridgeTwo(String contextString) {
      this.contextString = contextString;
   }

   @BridgeMethod
   public void decorate(GoodBridgeTarget goodDecoratee){
      goodDecoratee
        .completeSentence(contextString)
        .completeSentence(DECORATION);
   }
}
