package com.panid.pojos.bridge.bad;

import java.util.Objects;

public class BadBridgeTarget {

   private String sentence;

   public String getSentence() {
      return sentence;
   }

   public void setSentence(String sentence) {
      this.sentence = sentence;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      BadBridgeTarget decoratee = (BadBridgeTarget) o;
      return Objects.equals(sentence, decoratee.sentence);
   }

   @Override
   public int hashCode() {
      return Objects.hash(sentence);
   }
}
