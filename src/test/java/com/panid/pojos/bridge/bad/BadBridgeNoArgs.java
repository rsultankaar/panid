package com.panid.pojos.bridge.bad;

import com.panid.structural.Bridge;
import com.panid.structural.Bridge.BridgeMethod;

@Bridge
public class BadBridgeNoArgs {

   @BridgeMethod
   public void decorate(){}
}
