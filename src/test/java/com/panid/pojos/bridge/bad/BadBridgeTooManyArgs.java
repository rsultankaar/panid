package com.panid.pojos.bridge.bad;

import com.panid.pojos.bridge.good.GoodBridgeTarget;
import com.panid.structural.Bridge;
import com.panid.structural.Bridge.BridgeMethod;

@Bridge
public class BadBridgeTooManyArgs {

   @BridgeMethod
   public void decorate(GoodBridgeTarget goodDecoratee, String aString){}
}
