package com.panid.pojos.bridge.bad;

import com.panid.structural.Bridge;

@Bridge
public class BadBridgeNoMethod {

   public void decorate(){}
}
