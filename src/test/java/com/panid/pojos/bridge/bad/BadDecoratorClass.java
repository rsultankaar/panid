package com.panid.pojos.bridge.bad;

import com.panid.structural.Bridge.BridgeMethod;

public class BadDecoratorClass {

   @BridgeMethod
   public void decorate(){}
}
