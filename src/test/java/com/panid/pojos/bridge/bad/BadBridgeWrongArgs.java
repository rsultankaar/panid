package com.panid.pojos.bridge.bad;

import com.panid.structural.Bridge;
import com.panid.structural.Bridge.BridgeMethod;

@Bridge
public class BadBridgeWrongArgs {

   @BridgeMethod
   public void decorate(String string){}
}
