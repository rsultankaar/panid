package com.panid.pojos.factory;

import com.panid.creational.Factory;
import com.panid.pojos.factory.manufactuable.ProductA;
import com.panid.pojos.factory.manufactuable.ProductB;
import com.panid.pojos.factory.manufactuable.impl.ProductA1;
import com.panid.pojos.factory.manufactuable.impl.ProductA2;
import com.panid.pojos.factory.manufactuable.impl.ProductB1;
import com.panid.pojos.factory.manufactuable.impl.ProductB2;

@Factory(entities = {ProductA.class, ProductB.class, Integer.class})
public class StandardFactory {

  @Factory.FactoryMethod
  public ProductA manufactureProductA(boolean choose) {
    return choose ? new ProductA1() : new ProductA2();
  }

  @Factory.FactoryMethod
  public ProductB manufactureProductB(boolean choose) {
    return choose ? new ProductB1() : new ProductB2();
  }

  public Integer manufactureInteger() {
    return 0;
  }

  @Factory.FactoryMethod
  public Double manufactureInfinity() {
    return Double.POSITIVE_INFINITY;
  }
}
