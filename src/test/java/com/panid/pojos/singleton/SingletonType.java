package com.panid.pojos.singleton;

import com.panid.creational.Singleton;

@Singleton
public class SingletonType {

  private final String value;

  public SingletonType() {
    this("");
  }

  public SingletonType(String value) {
    this.value = value;
  }
}
