package com.panid.pojos.builder;

import com.panid.creational.Builder;

@Builder
public class VoidBuilder {

  @Builder.BuildMethod
  public void build(String aString) {}
}
