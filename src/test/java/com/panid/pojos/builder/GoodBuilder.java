package com.panid.pojos.builder;

import com.google.common.base.Strings;
import com.panid.creational.Builder;

@Builder
public class GoodBuilder {

  public static final String HAS_BUILT_WITHOUT_STRING = "Has built without string.";
  public static final String HAS_BUILT_WITH_STRING = "Has built with string :";

  @Builder.BuildMethod
  public String build(String aString) {
    return Strings.isNullOrEmpty(aString)
        ? HAS_BUILT_WITHOUT_STRING
        : HAS_BUILT_WITH_STRING + aString;
  }
}
