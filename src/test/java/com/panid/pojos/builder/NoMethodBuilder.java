package com.panid.pojos.builder;

import com.google.common.base.Strings;
import com.panid.creational.Builder;

@Builder
public class NoMethodBuilder {

  public String build(String aString) {
    return Strings.isNullOrEmpty(aString)
        ? GoodBuilder.HAS_BUILT_WITHOUT_STRING
        : GoodBuilder.HAS_BUILT_WITH_STRING + aString;
  }
}
