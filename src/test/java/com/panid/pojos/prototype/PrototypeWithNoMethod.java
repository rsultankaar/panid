package com.panid.pojos.prototype;

import com.panid.creational.Prototype;

@Prototype
public class PrototypeWithNoMethod {}
