package com.panid.pojos.prototype;

import com.panid.creational.Prototype;
import com.panid.creational.Prototype.PrototypeMethod;
import java.util.UUID;

@Prototype
public class PrototypeWithMethod {

  private final UUID uuid;

  public PrototypeWithMethod(UUID uuid) {
    this.uuid = uuid;
  }

  @PrototypeMethod
  public PrototypeWithMethod customClone() {
    return new PrototypeWithMethod(uuid);
  }

  public UUID getUuid() {
    return uuid;
  }
}
