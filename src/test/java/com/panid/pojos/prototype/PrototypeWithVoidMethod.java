package com.panid.pojos.prototype;

import com.panid.creational.Prototype;
import com.panid.creational.Prototype.PrototypeMethod;

@Prototype
public class PrototypeWithVoidMethod {

  @PrototypeMethod
  public void voidClone() {}
}
