package com.panid.pojos.prototype;

import com.panid.creational.Prototype;
import java.util.UUID;

@Prototype
public class PrototypeClonable implements Cloneable {

  private final UUID uuid;

  public PrototypeClonable(UUID uuid) {
    this.uuid = uuid;
  }

  @Override
  public PrototypeClonable clone() throws CloneNotSupportedException {
    return (PrototypeClonable) super.clone();
  }

  public UUID getUuid() {
    return uuid;
  }
}
