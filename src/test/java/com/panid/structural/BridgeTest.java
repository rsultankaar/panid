package com.panid.structural;

import com.panid.core.engine.PatternEngine;
import com.panid.core.engine.StructuralPatternEngine;
import com.panid.core.executors.structural.BridgeExecutor;
import com.panid.core.indexer.StructuralPatternIndexer;
import com.panid.core.scanner.PackageScannerImpl;
import com.panid.pojos.bridge.bad.*;
import com.panid.pojos.bridge.good.GoodBridgeTarget;
import com.panid.pojos.bridge.good.GoodBridgeOne;
import com.panid.pojos.bridge.good.GoodBridgeTwo;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.util.NoSuchElementException;

import static com.panid.core.engine.BasePatternEngine.NON_INDEXED_EXCEPTION_MESSAGE;
import static com.panid.core.executors.structural.BridgeExecutor.NO_BRIDGE_MESSAGE;
import static java.lang.String.format;
import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(BlockJUnit4ClassRunner.class)
public class BridgeTest {

  private static final String COM_PANID_POJOS = "com.panid.pojos";

  @Rule
  public ExpectedException expectedException = ExpectedException.none();
  private PatternEngine patternEngine;

  @Before
  public void setUp() {
    var packageScanner = new PackageScannerImpl(COM_PANID_POJOS);
    var patternIndexer = new StructuralPatternIndexer(packageScanner);
    patternEngine = new StructuralPatternEngine(patternIndexer);
  }

  @Test
  public void must_decorate_decoratee_correctly() {
    var source = new GoodBridgeTarget();

    String context = " and ";
    patternEngine.execute(source, GoodBridgeOne.class, new GoodBridgeTwo(context));

    assertThat(source.getWords()).isNotEmpty()
      .contains(GoodBridgeOne.DECORATION , context , GoodBridgeTwo.DECORATION);
  }

  @Test
  public void must_decorate_decoratee_correctly_even_with_null_arguments() {
    var source = new GoodBridgeTarget();

    patternEngine.execute(source, GoodBridgeOne.class, null);

    assertThat(source.getWords()).isNotEmpty().contains(GoodBridgeOne.DECORATION);
  }

  @Test
  public void must_throw_IllegalArgumentException_due_to_empty_args() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage(NO_BRIDGE_MESSAGE);

    patternEngine.execute(new GoodBridgeTarget());
  }

  @Test
  public void must_throw_IllegalArgumentException_due_to_null() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage(NO_BRIDGE_MESSAGE);

    patternEngine.execute(new GoodBridgeTarget());
  }

  @Test
  public void must_throw_IllegalArgumentException_due_irrelevant_args() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage(NO_BRIDGE_MESSAGE);

    patternEngine.execute(new GoodBridgeTarget(), String.class, "toto");
  }

  @Test
  public void must_throw_IllegalArgumentException_due_null_args() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage(NO_BRIDGE_MESSAGE);

    patternEngine.execute(new GoodBridgeTarget(),  null, null);
  }

  @Test
  public void must_throw_IllegalArgumentException_due_null_array_arg() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage(NO_BRIDGE_MESSAGE);

    patternEngine.execute(new GoodBridgeTarget(),  null);
  }

  @Test
  public void must_throw_NoSuchElementException_due_to_bad_decoratee_class() {
    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(format(NON_INDEXED_EXCEPTION_MESSAGE, BadBridgeTarget.class.getCanonicalName()));

    patternEngine.execute(new BadBridgeTarget());
  }

  @Test
  public void must_throw_IllegalArgumentException_due_to_bad_decorator_clas() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage(NO_BRIDGE_MESSAGE);

    patternEngine.execute(new GoodBridgeTarget(),
      BadDecoratorClass.class, BadBridgeNoMethod.class, BadBridgeWrongArgs.class
    );
  }

  @Test
  public void must_throw_NoSuchElementException_due_to_no_decorator_args() {
    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(BridgeExecutor.ERROR_MESSAGE);

    patternEngine.execute(new GoodBridgeTarget(), BadBridgeNoArgs.class);
  }

  @Test
  public void must_throw_IllegalArgumentException_due_to_too_many_decorator_args() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage("wrong number of arguments");

    patternEngine.execute(new GoodBridgeTarget(), BadBridgeTooManyArgs.class);
  }
}