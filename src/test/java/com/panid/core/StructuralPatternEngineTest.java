package com.panid.core;

import com.panid.core.engine.PatternEngine;
import com.panid.core.engine.StructuralPatternEngine;
import com.panid.core.indexer.StructuralPatternIndexer;
import com.panid.core.scanner.PackageScannerImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.util.NoSuchElementException;

import static com.panid.core.engine.BasePatternEngine.NON_INDEXED_EXCEPTION_MESSAGE;
import static java.lang.String.format;

@RunWith(BlockJUnit4ClassRunner.class)
public class StructuralPatternEngineTest {

  private static final String COM_PANID_POJOS = "com.panid.pojos";

  @Rule public ExpectedException expectedException = ExpectedException.none();
  private PatternEngine patternEngine;

  @Before
  public void setUp() {
    var packageScanner = new PackageScannerImpl(COM_PANID_POJOS);
    var patternIndexer = new StructuralPatternIndexer(packageScanner);
    patternEngine = new StructuralPatternEngine(patternIndexer);
  }

  @Test
  public void must_throw_NoSuchElementException_due_to_non_indexed_type() {
    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(
        format(NON_INDEXED_EXCEPTION_MESSAGE, String.class.getCanonicalName()));

    patternEngine.execute("");
  }

  @Test
  public void must_throw_exception_due_to_null_object() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage("Source object must be not null");

    patternEngine.execute(null);
  }
}
