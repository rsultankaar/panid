package com.panid.core;

import static com.panid.core.engine.BasePatternEngine.NON_INDEXED_EXCEPTION_MESSAGE;
import static java.lang.String.format;

import com.panid.core.engine.CreationalPatternEngine;
import com.panid.core.engine.PatternEngine;
import com.panid.core.indexer.CreationalPatternIndexer;
import com.panid.core.scanner.PackageScannerImpl;
import com.panid.core.type.CreationalType;
import java.util.NoSuchElementException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class CreationalPatternEngineTest {

  private static final String COM_PANID_POJOS = "com.panid.pojos";

  @Rule public ExpectedException expectedException = ExpectedException.none();
  private PatternEngine patternsExecutor;

  @Before
  public void setUp() {
    var packageScanner = new PackageScannerImpl(COM_PANID_POJOS);
    var patternIndexer = new CreationalPatternIndexer(packageScanner);
    patternsExecutor = new CreationalPatternEngine(patternIndexer);
  }

  @Test
  public void must_throw_NoSuchElementException_due_to_non_indexed_type() {
    expectedException.expect(NoSuchElementException.class);
    expectedException.expectMessage(
        format(NON_INDEXED_EXCEPTION_MESSAGE, String.class.getCanonicalName()));

    patternsExecutor.execute("");
  }

  @Test
  public void must_throw_exception_due_to_null_object() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage("Source object must be not null");

    patternsExecutor.execute(null);
  }
}
