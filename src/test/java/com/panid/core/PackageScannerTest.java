package com.panid.core;

import static com.panid.core.scanner.PackageScannerImpl.REFLECTIONS_PATTERN;
import static java.lang.System.getProperties;
import static org.fest.assertions.api.Assertions.assertThat;

import com.panid.core.scanner.PackageScannerImpl;
import com.panid.creational.Builder;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.reflections.Reflections;

@RunWith(BlockJUnit4ClassRunner.class)
public class PackageScannerTest {

  public static final String COM_PANID_POJOS = "com.panid.pojos";
  private Properties properties;

  @Before
  public void setUp() {
    getProperties().put(REFLECTIONS_PATTERN, COM_PANID_POJOS);

    properties = System.getProperties();
    properties.put(REFLECTIONS_PATTERN, COM_PANID_POJOS);
  }

  @Test
  public void must_return_reflection_object_with_right_number_of_types_with_properties() {
    var packageScanner = new PackageScannerImpl(properties);
    Reflections reflections = packageScanner.scan();

    assertThat(reflections).isNotNull();
    assertThat(reflections.getTypesAnnotatedWith(Builder.class)).isNotEmpty().hasSize(3);
  }

  @Test
  public void must_return_reflection_object_with_right_number_of_types_with_string() {
    var packageScanner = new PackageScannerImpl(COM_PANID_POJOS);
    Reflections reflections = packageScanner.scan();

    assertThat(reflections).isNotNull();
    assertThat(reflections.getTypesAnnotatedWith(Builder.class)).isNotEmpty().hasSize(3);
  }

  @Test
  public void must_return_reflection_object_with_right_number_of_types_with_system_properties() {
    var packageScanner = new PackageScannerImpl();
    Reflections reflections = packageScanner.scan();

    assertThat(reflections).isNotNull();
    assertThat(reflections.getTypesAnnotatedWith(Builder.class)).isNotEmpty().hasSize(3);
  }

  @After
  public void cleanUp() {
    getProperties().put(REFLECTIONS_PATTERN, "");
  }
}
