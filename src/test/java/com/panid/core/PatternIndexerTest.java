package com.panid.core;

import static org.fest.assertions.api.Assertions.assertThat;

import com.panid.core.indexer.CreationalPatternIndexer;
import com.panid.core.scanner.PackageScannerImpl;
import com.panid.core.type.CreationalType;
import com.panid.pojos.builder.GoodBuilder;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class PatternIndexerTest {

  private static final String COM_PANID_POJOS = "com.panid.pojos";

  @Test
  public void should_index_only_submitted_class_with_defined_pattern() {
    var packageScanner = new PackageScannerImpl(COM_PANID_POJOS);
    var patternIndexer = new CreationalPatternIndexer(packageScanner);

    Map<Class<?>, CreationalType> map = patternIndexer.getIndex();
    assertThat(map.entrySet()).isNotNull().isNotEmpty().hasSize(10);
    assertThat(map.get(GoodBuilder.class)).isNotNull().isEqualTo(CreationalType.BUILDER);
  }
}
