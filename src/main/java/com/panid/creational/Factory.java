package com.panid.creational;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Factory {

  Class<?>[] entities() default {Object.class};

  @Target(ElementType.METHOD)
  @Retention(RetentionPolicy.RUNTIME)
  @interface FactoryMethod {}
}
