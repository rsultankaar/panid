package com.panid.core.engine;

public interface PatternEngine {

  <RETURN_TYPE> RETURN_TYPE execute(Object source, Object... args);
}
