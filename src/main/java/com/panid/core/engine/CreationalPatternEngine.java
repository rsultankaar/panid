package com.panid.core.engine;

import static java.util.Arrays.copyOfRange;

import com.panid.core.executors.creational.BuilderExecutor;
import com.panid.core.executors.creational.FactoryExecutor;
import com.panid.core.executors.creational.PrototypeExecutor;
import com.panid.core.executors.creational.SingletonExecutor;
import com.panid.core.indexer.IPatternIndexer;
import com.panid.core.type.CreationalType;

import java.util.NoSuchElementException;

public class CreationalPatternEngine extends BasePatternEngine<CreationalType> {

   private static final String NOT_IMPLEMENTED_TYPE_MESSAGE =
     "The CreationalType you submitted is not implemented";

   public CreationalPatternEngine(IPatternIndexer<CreationalType> patternIndexer) {
      super(patternIndexer);
   }

   protected <RETURN_TYPE> RETURN_TYPE execute(Object source, CreationalType type, Object[] args) {
      return switch (type) {
         case BUILDER -> new BuilderExecutor<RETURN_TYPE>().execute(source, args);
         case ABSTRACT_FACTORY -> this.getAbstractFactoryExecutor(source, args);
         case PROTOTYPE -> new PrototypeExecutor<RETURN_TYPE>().execute(source, args);
         case SINGLETON -> new SingletonExecutor<RETURN_TYPE>().execute(source, args);
         default -> throw new NoSuchElementException(NOT_IMPLEMENTED_TYPE_MESSAGE);
      };
   }

   private <T> T getAbstractFactoryExecutor(Object source, Object[] args) {
      Class<T> targetClass = args.length == 0 ? null : (Class<T>) args[0];
      return new FactoryExecutor<>(targetClass).execute(source, copyOfRange(args, 1, args.length));
   }
}
