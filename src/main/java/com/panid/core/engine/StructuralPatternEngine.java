package com.panid.core.engine;

import com.panid.core.executors.structural.BridgeExecutor;
import com.panid.core.indexer.IPatternIndexer;
import com.panid.core.type.StructuralType;

import java.util.NoSuchElementException;

public class StructuralPatternEngine extends BasePatternEngine<StructuralType> {

   private static final String NOT_IMPLEMENTED_TYPE_MESSAGE = "The StructuralType you submitted is not implemented";

   public StructuralPatternEngine(IPatternIndexer<StructuralType> patternIndexer) {
      super(patternIndexer);
   }

   protected <RETURN_TYPE> RETURN_TYPE execute(Object source, StructuralType type, Object[] args) {
      return switch (type) {
         case BRIDGE -> new BridgeExecutor<RETURN_TYPE>().execute(source, args);
         default -> throw new NoSuchElementException(NOT_IMPLEMENTED_TYPE_MESSAGE);
      };
   }
}
