package com.panid.core.engine;

import static java.lang.String.format;
import static java.util.Objects.nonNull;

import com.panid.core.indexer.IPatternIndexer;
import java.util.Map;
import java.util.NoSuchElementException;

public abstract class BasePatternEngine<PATTERN_TYPE> implements PatternEngine {

  public static final String NON_INDEXED_EXCEPTION_MESSAGE =
      "The object with type %s you submitted was not indexed.";
  private final IPatternIndexer patternIndex;

  BasePatternEngine(IPatternIndexer<PATTERN_TYPE> patternIndexer) {
    this.patternIndex = patternIndexer;
  }

  public <T> T execute(Object source, Object... args) {
    if (nonNull(source)) {
      Map<Class<?>, PATTERN_TYPE> index = patternIndex.getIndex();
      var type = index.get(getaClass(source));
      if (nonNull(type)) {
        return execute(source, type, args);
      }
      throw new NoSuchElementException(
          format(NON_INDEXED_EXCEPTION_MESSAGE, source.getClass().getCanonicalName()));
    }
    throw new IllegalArgumentException("Source object must be not null");
  }

  protected abstract <RETURN_TYPE> RETURN_TYPE execute(Object source, PATTERN_TYPE type, Object[] args);

  private Class<?> getaClass(Object source) {
    return source instanceof Class<?> ? (Class<?>) source : source.getClass();
  }
}
