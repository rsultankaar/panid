package com.panid.core.type;

public enum CreationalType {
  BUILDER,
  ABSTRACT_FACTORY,
  PROTOTYPE,
  SINGLETON
}
