package com.panid.core.invoke;

import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;

import java.lang.reflect.InvocationTargetException;

public class ConstructorInvoker implements Invoker {

  private static final String ARGUMENT_EXCEPTION =
      "args and classArgs must have the same length. "
          + "Check if you have null objects in your arguments";

  private final Class<?> sourceClass;
  private final Class<?>[] classArgs;

  public ConstructorInvoker(Class<?> sourceClass, Class<?>... classArgs) {
    requireNonNull(sourceClass, "ConstructorInvoker sourceClass should be not null");
    requireNonNull(classArgs, "ConstructorInvoker classArgs should be not null");
    this.sourceClass = sourceClass;
    this.classArgs = classArgs;
  }

  @Override
  public <T> T invoke(Object... args) {
    try {
      if (classArgs.length == 0) {
        return (T) this.sourceClass.getConstructor().newInstance();
      } else if (nonNull(args) && classArgs.length != args.length) {
        throw new IllegalArgumentException(ARGUMENT_EXCEPTION);
      }
      return (T) this.sourceClass.getConstructor(classArgs).newInstance(args);

    } catch (NoSuchMethodException
        | InstantiationException
        | IllegalAccessException
        | InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }
}
