package com.panid.core.invoke;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodInvoker implements Invoker {

  private final Object object;
  private final Method method;

  public MethodInvoker(Object object, Method method) {
    requireNonNull(object, "MethodInvoker object should be not null");
    requireNonNull(object, "MethodInvoker method should be not null");
    this.object = object;
    this.method = method;
  }

  public <T> T invoke(Object... args) {
    try {
      if (method.getParameterCount() > 0) {
        if (isNull(args) || args.length == 0) {
          return (T) method.invoke(object, (Object) null);
        }
        return (T) method.invoke(object, args);
      }
      return (T) method.invoke(object);
    } catch (IllegalAccessException | InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }
}
