package com.panid.core.invoke;

public interface Invoker {

  <T> T invoke(Object[] args);
}
