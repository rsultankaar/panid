package com.panid.core.scanner;

import org.reflections.Reflections;

public interface PackageScanner {

  Reflections scan();
}
