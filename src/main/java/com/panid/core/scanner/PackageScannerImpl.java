package com.panid.core.scanner;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

import java.util.Properties;
import org.reflections.Reflections;

public class PackageScannerImpl implements PackageScanner {

  private Reflections reflections;
  private final Properties properties;

  public static final String REFLECTIONS_PATTERN = "reflections.pattern";
  public static final String DEFAULT_VALUE = "*";

  public PackageScannerImpl() {
    this(System.getProperties());
  }

  public PackageScannerImpl(String value) {
    this(propertiesFromString(value));
  }

  private static Properties propertiesFromString(String value) {
    Properties properties = new Properties();
    properties.put(REFLECTIONS_PATTERN, value);
    return properties;
  }

  public PackageScannerImpl(Properties properties) {
    requireNonNull(properties, "Properties argument should be not null");
    this.properties = properties;
  }

  @Override
  public Reflections scan() {
    if (isNull(reflections)) {
      String pattern = properties.getProperty(REFLECTIONS_PATTERN, DEFAULT_VALUE);
      reflections = new Reflections(pattern);
    }
    return reflections;
  }
}
