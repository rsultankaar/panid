package com.panid.core;

import static com.panid.core.type.CreationalType.*;
import static com.panid.core.type.StructuralType.BRIDGE;

import com.panid.core.type.CreationalType;
import com.panid.core.type.StructuralType;
import com.panid.creational.Builder;
import com.panid.creational.Factory;
import com.panid.creational.Prototype;
import com.panid.creational.Singleton;

import com.panid.structural.Bridge.BridgeTarget;
import java.lang.annotation.Annotation;
import java.util.Map;

public final class Annotations {

  private static final Map<CreationalType, Class<? extends Annotation>> creationalTypes =
      Map.ofEntries(
          Map.entry(BUILDER, Builder.class),
          Map.entry(ABSTRACT_FACTORY, Factory.class),
          Map.entry(PROTOTYPE, Prototype.class),
          Map.entry(SINGLETON, Singleton.class));

  private static final Map<StructuralType, Class<? extends Annotation>> structuralTypes =
      Map.ofEntries(
        Map.entry(BRIDGE, BridgeTarget.class)
      );

  public static Map<CreationalType, Class<? extends Annotation>> creationals() {
    return creationalTypes;
  }

  public static Map<StructuralType, Class<? extends Annotation>> structurals() {
    return structuralTypes;
  }
}
