package com.panid.core.indexer;

import static java.util.Objects.isNull;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;
import org.reflections.Reflections;

public abstract class PatternIndexer<T> implements IPatternIndexer<T> {

  Map<Class<?>, T> index;

  Stream<Entry<Class<?>, T>> TO_INDEX(
      Reflections reflections, Entry<T, Class<? extends Annotation>> entry) {
    return reflections.getTypesAnnotatedWith(entry.getValue()).stream()
        .map(clazz -> Map.entry(clazz, entry.getKey()));
  }

  @Override
  public Map<Class<?>, T> getIndex() {
    if (isNull(index)) this.index();
    return index;
  }
}
