package com.panid.core.indexer;

import static java.util.Optional.ofNullable;

import com.panid.core.Annotations;
import com.panid.core.scanner.PackageScanner;
import com.panid.core.type.CreationalType;
import java.util.Map.Entry;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.reflections.Reflections;

public class CreationalPatternIndexer extends PatternIndexer<CreationalType> {

  private static final String INDEXER_MESSAGE = "Pattern Indexer should be not null";
  private static final Supplier<IllegalArgumentException> EXCEPTION_SUPPLIER =
      () -> new IllegalArgumentException(INDEXER_MESSAGE);
  private final PackageScanner patternScanner;

  public CreationalPatternIndexer(PackageScanner packageScanner) {
    this.patternScanner = packageScanner;
  }

  @Override
  public void index() {
    var packageScanner = ofNullable(this.patternScanner).orElseThrow(EXCEPTION_SUPPLIER);
    Reflections reflections = packageScanner.scan();

    var annotations = Annotations.creationals().entrySet();
    index =
        annotations.stream()
            .flatMap(entry -> TO_INDEX(reflections, entry))
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
  }
}
