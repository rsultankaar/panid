package com.panid.core.indexer;

import java.util.Map;

public interface IPatternIndexer<PATTERN_TYPE> {

  void index();

  Map<Class<?>, PATTERN_TYPE> getIndex();
}
