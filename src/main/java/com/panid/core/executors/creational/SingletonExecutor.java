package com.panid.core.executors.creational;

import static java.util.Objects.nonNull;

import com.panid.core.executors.PatternExecutor;
import com.panid.core.invoke.ConstructorInvoker;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public final class SingletonExecutor<T> implements PatternExecutor<T> {

  private static final String SOURCE_OBJECT_MUST_BE_NOT_NULL = "Source object must be not null";
  private static final Map<Class<?>, Object> singletons = new HashMap<>();

  @Override
  public T execute(Object source, Object... args) {
    if (nonNull(source)) {
      return (T) getSingleton((Class<T>) source, args);
    }
    throw new IllegalArgumentException(SOURCE_OBJECT_MUST_BE_NOT_NULL);
  }

  private Object getSingleton(Class<T> sourceClass, Object[] args) {
    return singletons.computeIfAbsent(
        sourceClass,
        clazz -> new ConstructorInvoker(sourceClass, getArgClasses(args)).invoke(args));
  }

  private Class<?>[] getArgClasses(Object[] args) {
    return Stream.of(args).filter(Objects::nonNull).map(Object::getClass).toArray(Class<?>[]::new);
  }
}
