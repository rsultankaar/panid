package com.panid.core.executors.creational;

import com.panid.core.executors.TargetPatternExecutor;
import com.panid.core.invoke.MethodInvoker;
import com.panid.creational.Builder.BuildMethod;
import java.lang.reflect.Method;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class BuilderExecutor<T> extends TargetPatternExecutor<T> {

  private static Predicate<Method> HAS_BUILDER_METHOD_ANNOTATION =
      isSpecifiedAnnotatedMethod(BuildMethod.class);

  @Override
  public T execute(Object source, Object... args) {
    var clazz = source.getClass();

    var buildMethod =
        Stream.of(clazz.getMethods())
            .filter(HAS_BUILDER_METHOD_ANNOTATION)
            .filter(method -> !method.getReturnType().equals(Void.TYPE))
            .findFirst()
            .orElseThrow(getExceptionSupplier(clazz, BuildMethod.class));

    return new MethodInvoker(source, buildMethod).invoke(args);
  }
}
