package com.panid.core.executors.creational;

import static com.panid.creational.Factory.FactoryMethod;

import com.panid.core.executors.TargetPatternExecutor;
import com.panid.core.invoke.MethodInvoker;
import com.panid.creational.Factory;
import java.lang.reflect.Method;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class FactoryExecutor<T> extends TargetPatternExecutor<T> {

  private static final Predicate<Method> IS_FACTORY_METHOD =
      isSpecifiedAnnotatedMethod(FactoryMethod.class);

  public FactoryExecutor(Class<T> targetClass) {
    super(targetClass);
  }

  @Override
  public T execute(Object source, Object... args) {
    var sourceClass = source.getClass();
    var annotation = sourceClass.getAnnotation(Factory.class);
    var factoryClasses = List.of(annotation.entities());

    var factoryMethod =
        Stream.of(sourceClass.getMethods())
            .filter(IS_FACTORY_METHOD)
            .filter(IS_NOT_VOID_TYPE)
            .filter(hasSameTargetType())
            .filter(isInFactoryScope(factoryClasses))
            .findFirst()
            .orElseThrow(getExceptionSupplier(sourceClass, FactoryMethod.class));

    return new MethodInvoker(source, factoryMethod).invoke(args);
  }

  private Predicate<Method> isInFactoryScope(List<Class<?>> factoryClasses) {
    return method -> factoryClasses.contains(method.getReturnType());
  }
}
