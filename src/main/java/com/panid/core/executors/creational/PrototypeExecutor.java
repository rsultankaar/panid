package com.panid.core.executors.creational;

import static java.lang.String.format;

import com.panid.core.executors.TargetPatternExecutor;
import com.panid.core.invoke.MethodInvoker;
import com.panid.creational.Prototype.PrototypeMethod;
import java.lang.reflect.Method;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class PrototypeExecutor<T> extends TargetPatternExecutor<T> {

  private static final String ERROR_MESSAGE =
      ". Please annotate a non void method with @%s or implement clone() method.";
  private static final Predicate<Method> IS_PROTOTYPE_METHOD =
      isSpecifiedAnnotatedMethod(PrototypeMethod.class);

  @Override
  public T execute(Object source, Object... args) {
    var sourceClass = source.getClass();
    var prototypeMethod =
        Stream.of(sourceClass.getMethods())
            .filter(IS_NOT_VOID_TYPE)
            .filter(IS_PROTOTYPE_METHOD)
            .filter(returnTypeEqualsSource(source))
            .findFirst()
            .orElseGet(() -> getCloneMethod(sourceClass));
    return new MethodInvoker(source, prototypeMethod).invoke(args);
  }

  private Predicate<Method> returnTypeEqualsSource(Object source) {
    return method -> method.getReturnType().isInstance(source);
  }

  private static final String CLONE_METHOD_NAME = "clone";

  private Method getCloneMethod(Class<?> sourceClass) {
    try {
      return sourceClass.getMethod(CLONE_METHOD_NAME);
    } catch (NoSuchMethodException e) {
      String errorMessage = format(ERROR_MESSAGE, PrototypeMethod.class);
      throw getExceptionSupplier(sourceClass, PrototypeMethod.class, errorMessage).get();
    }
  }
}
