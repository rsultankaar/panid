package com.panid.core.executors;

public interface PatternExecutor<T> {

  T execute(Object source, Object... args) throws IllegalAccessException;
}
