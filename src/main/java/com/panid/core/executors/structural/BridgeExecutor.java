package com.panid.core.executors.structural;

import static java.util.Arrays.*;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toMap;

import com.panid.core.executors.TargetPatternExecutor;
import com.panid.core.invoke.ConstructorInvoker;
import com.panid.core.invoke.MethodInvoker;
import com.panid.exception.NoSuchElementSupplier;
import com.panid.structural.Bridge;
import com.panid.structural.Bridge.BridgeMethod;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class BridgeExecutor<T> extends TargetPatternExecutor<T> {

   public static final String NO_BRIDGE_MESSAGE = "There was no bridge fulfilling the requirements";
   public static final String ERROR_MESSAGE = "Method should have at least one parameter";

   private static final NoSuchElementSupplier EXCEPTION_SUPPLIER =
     new NoSuchElementSupplier(ERROR_MESSAGE);

   private static final Predicate<Entry<Class<?>, Object>> HAS_BRIDGE_ANNOTATION = kv -> {
      Annotation[] annotations = kv.getKey().getAnnotations();
      return annotations.length > 0 &&
        stream(annotations).anyMatch(annotation -> annotation instanceof Bridge);
   };

   private static final Predicate<Entry<Object, Method>> IS_BRIDGE_METHOD =
     kv -> isSpecifiedAnnotatedMethod(BridgeMethod.class).test(kv.getValue());

   private static final Function<Object, Entry<Class<?>, Object>> SETUP_DECORATOR_CANDIDATES =
     object ->
       object instanceof Class<?>
         ? Map.entry((Class<?>) object, new ConstructorInvoker((Class<?>) object).invoke())
         : Map.entry(object.getClass(), object);

   private static final Function<Entry<Class<?>, Object>, Stream<Entry<Object, Method>>> WITH_CANDIDATE_METHOD =
     kv -> stream(kv.getKey().getMethods()).map(method -> Map.entry(kv.getValue(), method));


   private Predicate<Entry<Object, Method>> CAN_BRIDGE(Object decoree) {
      return kv -> stream(kv.getValue().getParameterTypes()).findFirst()
        .orElseThrow(EXCEPTION_SUPPLIER)
        .equals(decoree.getClass());
   }

   @Override
   public T execute(Object bridgeTarget, Object... bridges) {
      if (nonNull(bridges)) {
         var objectMethodMap = stream(bridges).filter(Objects::nonNull)
           .map(SETUP_DECORATOR_CANDIDATES)
           .filter(HAS_BRIDGE_ANNOTATION)
           .flatMap(WITH_CANDIDATE_METHOD)
           .filter(kv -> IS_VOID_TYPE.test(kv.getValue()))
           .filter(IS_BRIDGE_METHOD)
           .filter(CAN_BRIDGE(bridgeTarget))
           .collect(toMap(Entry::getKey, Entry::getValue, (kv1, kv2) -> kv1));

         if (objectMethodMap.size() > 0) {
            objectMethodMap.forEach(
              (obj, method) -> new MethodInvoker(obj, method).invoke(bridgeTarget)
            );
            return (T) bridgeTarget;
         }
      }
      throw new IllegalArgumentException(NO_BRIDGE_MESSAGE);
   }
}