package com.panid.core.executors;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;

import com.panid.exception.NoSuchElementSupplier;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.NoSuchElementException;
import java.util.function.Predicate;
import java.util.function.Supplier;

public abstract class TargetPatternExecutor<T> implements PatternExecutor<T> {

  private final Class<T> targetClass;

  public TargetPatternExecutor() {
    targetClass = null;
  }

  protected TargetPatternExecutor(Class<T> targetClass) {
    this.targetClass = targetClass;
    requireNonNull(targetClass, "targetClass must be not null");
  }

  protected static final Predicate<Method> IS_VOID_TYPE =
      method -> method.getReturnType().equals(Void.TYPE);
  protected static final Predicate<Method> IS_NOT_VOID_TYPE = IS_VOID_TYPE.negate();

  protected final Predicate<Method> hasSameTargetType() {
    return method -> method.getReturnType().equals(targetClass);
  }

  protected static Predicate<Method> isSpecifiedAnnotatedMethod(Class<? extends Annotation> annotationClass) {
    return method -> {
      try {
        return nonNull(method.getAnnotation(annotationClass));
      } catch (ClassCastException e) {
        return false;
      }
    };
  }

  public static final String ERROR_MESSAGE = "There was no method annotated %s.class in %s.class";
  public static final String WITH_TARGET = " with returnType %s";

  protected Supplier<NoSuchElementException> getExceptionSupplier(
      Class<?> clazz, Class<? extends Annotation> annotationClass) {
    return getExceptionSupplier(clazz, annotationClass, null);
  }

  protected Supplier<NoSuchElementException> getExceptionSupplier(
      Class<?> clazz, Class<? extends Annotation> annotationClass, String extraMessage) {
    return new NoSuchElementSupplier(
        format(ERROR_MESSAGE, annotationClass.getCanonicalName(), clazz.getCanonicalName())
            + (nonNull(targetClass) ? format(WITH_TARGET, targetClass.getCanonicalName()) : "")
            + (nonNull(extraMessage) ? extraMessage : ""));
  }
}
