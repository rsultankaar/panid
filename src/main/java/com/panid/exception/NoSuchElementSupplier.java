package com.panid.exception;

import java.util.NoSuchElementException;
import java.util.function.Supplier;

public class NoSuchElementSupplier implements Supplier<NoSuchElementException> {

  private final String message;

  public NoSuchElementSupplier(String message) {
    this.message = message;
  }

  @Override
  public NoSuchElementException get() {
    return new NoSuchElementException(message);
  }
}
